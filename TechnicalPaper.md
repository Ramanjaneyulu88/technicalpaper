## Document Object Model ## 

### The Abstract ### 

The following paper aims at explaining  the Document Object Model(DOM) that is a programming interface for web documents , how does DOM helps , how to access it and what are JS DOM Helper methods.It's also tries to explain the limits and adavantages and advancements of DOM.

### The Introduction ### 

DOM stands for Document Object Model. It is a programming interface that allows us to create, change, or remove elements from the document. We can also add events to these elements to make our page more dynamic.

The points that are discussed are 
1. What is DOM ?
2. How does DOM helps ?
3. How we can access DOM ?
4. What are DOM Helper methods?

### The Body ### 

**DOM:** 
The Document Object Model (DOM) is a programming interface for web documents. It represents the page so that programs can change the document structure, style, and content. The DOM represents the document as nodes and objects; that way, programming languages can interact with the page.


---
**NOTE**

The DOM views an HTML document as a tree of nodes. A node represents an HTML element.

---


**Advantages of DOM**

1. Data persists in memory
1. You can go forwards and backwards in the tree (random access)
1. You can make changes directly to the tree in memory
4. The file is parsed only once.
5. High navigation abilities : This is the aim of the DOM design.
6. Language and platform independent.
7. It is traversable - Move back and forth in data.
8. It is editable and dynamic.

**Accessing of DOM:**         
  There are different of accessing DOM. We can use Javascript to access DOM. The different methods are 
1. Get HTML element by Id
2. Get HTML element by className
3. Get HTML element by Name
1. Get HTML element by tagName
1. Get HTML element by CSS Selector

**What are DOM Helper Methods** 

    A DOM Helper is basically a wrapper for the DOM and DOM Elements (html/xml, but usually html) that solves cross browser discrepancies and adds higher level methods making dealing with the DOM a nicer experience.

Some expamples of Helper Methods: 
1. The forEach helper
1. The map helper
1. The find helper
1. The filter helper
1. The every helper
1. The some helper
1. The reduce helper



**The Conclusion**

DOM stands for Document Object Model and is a programming interface that allows us to create, change or remove elements from the document. We can also add events to these elements to make our page more dynamic.

You can select elements in JavaScript using methods like **getElementById()**, **querySelector()**, and **querySelectorAll()**.

**References** 

1. https://jimfrenette.com/javascript/document/
2. https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
3. https://www.javatpoint.com/document-object-model
4. https://www.freecodecamp.org/news/what-is-the-dom-explained-in-plain-english/













